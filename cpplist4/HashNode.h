//
// Created by Alexey Korzh on 05.06.2020.
//

#ifndef CPPLIST4_HASHNODE_H
#define CPPLIST4_HASHNODE_H

template<typename K, typename V>
class HashNode
{
public:
    V value;
    K key;

    //Constructor of hashnode
    HashNode(V value, K key) : value(value), key(key) {}
};


#endif //CPPLIST4_HASHNODE_H
