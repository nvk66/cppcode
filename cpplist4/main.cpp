#include <iostream>
#include "HashMap.h"

int main()
{
    std::cout << "Hello, World!" << std::endl;
    HashMap<int, int> *hashMap = new HashMap<int, int>;
    hashMap->insertNode(1, 100);
    hashMap->insertNode(2, 200);
    hashMap->insertNode(3, 300);
    hashMap->insertNode(4, 400);
    hashMap->display();
    hashMap->size();
    hashMap->isEmpty();
    hashMap->clear();
    hashMap->isEmpty();
    return 0;
}
