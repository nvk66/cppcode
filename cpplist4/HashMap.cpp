//
// Created by Alexey Korzh on 05.06.2020.
//

#include <cstdlib>
#include "HashMap.h"

#include <iostream>
using namespace std;

template<typename K, typename V>
HashMap<K, V>::HashMap()
{
    capacity = 20;
    length = 0;
    array = new HashNode<K, V> *[capacity];

    for (int i = 0; i < capacity; i++)
        array[i] = NULL;

    //dummy node with value and key -1
    dummy = new HashNode<K, V>(-1, -1);
}

template<typename K, typename V>
int HashMap<K, V>::hashCode(K key)
{
    return key % capacity;
}

template<typename K, typename V>
void HashMap<K, V>::insertNode(K key, V value)
{
    HashNode<K, V> *temp = new HashNode<K, V>(key, value);

    int hashIndex = hashCode(key);
    //в поисках свободного места
    while (array[hashIndex] != NULL && array[hashIndex]->key != key && array[hashIndex]->key != -1)
    {
        hashIndex++;
        hashIndex %= capacity;
    }

    //если новое значение решило увеличить размер
    if (array[hashIndex] == NULL || array[hashIndex]->key == -1)
        length++;
    array[hashIndex] = temp;
}

template<typename K, typename V>
V HashMap<K, V>::deleteNode(K key)
{
    int hashIndex = hashCode(key);
    //поиск ключа
    while (array[hashIndex] != NULL)
    {
        //если нашли
        if (array[hashIndex]->key == key)
        {
            HashNode<K, V> *temp = array[hashIndex];
            //вставление искусственного идекса
            array[hashIndex] = dummy;
            //уменьшение размера
            length--;
            return temp->value;
        }
        hashIndex++;
        hashIndex %= capacity;
    }
    //а если не нашли пишем нулллллллл
    return NULL;
}

template<typename K, typename V>
V HashMap<K, V>::getValue(K key)
{
    int hashIndex = hashCode(key);
    //поиск ключа
    while (array[hashIndex] != NULL)
    {
        int counter = 0;
        //изегаю бесконечный цикл
        if (counter++ > capacity)
            return NULL;
        //если нашёл возвращает значение
        if (array[hashIndex]->key == key)
            return array[hashIndex]->value;
        hashIndex++;
        hashIndex %= capacity;
    }

    //а если не нашёли то нуллллллллл
    return NULL;
}

template<typename K, typename V>
int HashMap<K, V>::size()
{
    return length;
}

template<typename K, typename V>
bool HashMap<K, V>::isEmpty()
{
    return length == 0;
}

template<typename K, typename V>
void HashMap<K, V>::clear()
{
    length = 0;
}

template<typename K, typename V>
void HashMap<K, V>::display()
{
    for(int i=0 ; i<capacity ; i++)
    {
        if(array[i] != NULL && array[i]->key != -1)
            cout << "key = " << array[i]->key << "  value = " << array[i]->value << endl;
    }
}






