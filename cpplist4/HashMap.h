//
// Created by Alexey Korzh on 05.06.2020.
//

#ifndef CPPLIST4_HASHMAP_H
#define CPPLIST4_HASHMAP_H
#include "HashNode.h"

template<typename K, typename V>
class HashMap
{
private:
    //hash element array
    HashNode<K,V> **array;
    int capacity;
    //current length
    int length;
    //dummy node
    HashNode<K,V> *dummy;


public:
    HashMap();
    void insertNode(K key, V value);
    int hashCode(K key);
    V deleteNode(K key);
    V getValue(K key);
    int size();
    bool isEmpty();
    void clear();
    void display();
};


#endif //CPPLIST4_HASHMAP_H
